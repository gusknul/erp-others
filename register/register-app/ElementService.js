function Elements ($resource, API, $q, $rootScope) {

	this.database = null;
	this.elements = [];

	var resource = $resource(
		API.URL+'/elements/:id', 
		{
			id: '@id_categories_elements'
		},
		{}
	);

	return  {
		getAll: function(params) {

			var _this = this;

			return resource
				.get(params)
				.$promise
				.then(function (response) {
					return response.response;

				}, function(err){

					return [];
					
				});

		}

	}

}

Elements.$inject = ['$resource', 'API', '$q'];

app.service('Elements', Elements);