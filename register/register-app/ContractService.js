function Contracts ($resource, $q, API) {

	var resource = $resource(
		API.URL+'/contracts/:id', 
		{
			id: '@id_contracts'
		},
		{

			send: {
				method: 'POST',
				headers: {
					'Content-Type': undefined
				}
			}

		}
	);

	return  {

		send : function(payload){
			return resource
				.send(payload)
				.$promise
				.then(function (response) {
					return response.response;
				});
		}

	}

}

Contracts.$inject = ['$resource', '$q','API'];

app.service('Contracts',Contracts);