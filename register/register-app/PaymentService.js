function Payment ($resource, $q, API, SessionService,$http) {

	var resource = $resource(
		API.URL+'/clients/:id_clients/cards', 
		{
			id_clients: '@id_clients'
		},
		{	

			getCard: {
				method: 'GET',
				url : API.URL + '/clients/:id_clients/cards/:id_card' 
			},
			delete : {
				method : 'DELETE',
				url : API.URL + '/clients/:id_clients/cards/:id_card' 
			},
			setAsDefault : {
				method : 'POST',
				url : API.URL + '/clients/:id_clients/cards/:id_card/setAsDefault' 
			},
			getFees: {
				method: 'GET',
				url : API.URL + '/fees' 
			}

		}
	);

	return  {

		getAll: function() {
			return resource
					.get({"id_clients":SessionService.getIdClient()})
					.$promise
					.then(function(response){
						return response.response;
					});
		},

		create : function(token){
			return resource
				   .save({"id_clients":SessionService.getIdClient(),"token" : token})
				   .$promise
				   .then(function(response){
				   		return response;
				   });
		},

		delete : function(id_card){
			return resource
					.delete({"id_clients":SessionService.getIdClient(),"id_card":id_card})
					.$promise
					.then(function(response){
						return response;
					});
		},

		setAsDefault : function(id_card){
			return resource
					.setAsDefault({"id_clients":SessionService.getIdClient(),"id_card":id_card} , null)
					.$promise
					.then(function(response){
						return response;
					});
		},

		getFees : function(){
			var resp = {"success":true,"message":"OK","response":{"AMERICAN_EXPRESS":{"1":"2.00","3":"5.40","6":"8.40","9":"11.40","12":"14.40"},"MC":{"1":"0.00","3":"5.40","6":"8.40","9":"11.40","12":"14.40"},"VISA":{"1":"0.00","3":"5.40","6":"8.40","9":"11.40","12":"14.40"}}};
			//return resp.response;
			return  $q(function(resolve, reject) {
						resolve(resp.response);
					});
			/*return resource
					.getFees()
					.$promise
					.then(function(response){
						return response.response;
					});*/
		}

	}

}

Payment.$inject = ['$resource', '$q', 'API', 'SessionService','$http'];
app.service('Payment',Payment);