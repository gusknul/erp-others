import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'meses' })
export class MesesPipe implements PipeTransform {
    transform(dias: any): any {
        return Math.round(dias / 30.41666);
    }
}
