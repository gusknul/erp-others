import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {

  @Input() fieldType: string;
  @Input() formName: string;
  @Input() placeholder: string;
  @Input() label: string;
  @Input() parentForm: FormGroup;
  @Input() submitted = false;
  @Input() value;
  @Input() min;
  @Input() max;
  @Output() checkboxValue: EventEmitter<any> = new EventEmitter();
  @Input() options: any;
  @Output() changePago: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
    //
  }

  keepOrder = (a, b) => a;

  get f(): any {
    return this.parentForm.controls;
  }

  onCheckChange(checkboxValue, event): void {
    if (checkboxValue && event.target.checked) {
      this.checkboxValue.emit(
        {
          value: checkboxValue,
          checked: event.target.checked
        });
    }
  }

  optionChanged(value): void {
    this.changePago.emit(value);
  }

}
