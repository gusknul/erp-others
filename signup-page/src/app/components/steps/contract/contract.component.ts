import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
declare var Conekta: any;
import Swal from 'sweetalert2';
import { RegisterService } from '../../../services/register.service';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit, OnChanges {

  @Input() personalData: any;
  @Input() materialData: any;
  @Input() paymentData: any;
  @Input() client: any;
  @Input() fees: any;
  @Input() stepper: any;
  @Output() sumaTotal: EventEmitter<any> = new EventEmitter();
  extras = [];
  totalExtras = 0;
  respSocial = 0;
  respMensual = 0;
  sum = 0;

  constructor(private readonly registerService: RegisterService) { }

  ngOnInit(): void {
    //
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.materialData && changes.materialData.currentValue) {
      if (this.materialData.material) {
        const material = this.materialData.material;
        this.extras.push({ name: material.name, price: material.price });
        this.totalExtras += parseFloat(material.price);
      }
      if (this.materialData.elements) {
        for (const [key, value] of Object.entries(this.materialData.elements)) {
          // tslint:disable-next-line:no-string-literal
          this.extras.push({ name: value['name'], price: value['price'] });
          // tslint:disable-next-line:no-string-literal
          this.totalExtras += parseFloat(value['price']);
        }
      }
      if (this.materialData.responsabilidad) {
        this.respSocial = (this.totalExtras / 1.16) * 0.007;
        // this.respMensual = ((this.client.group.sign_into.monthly_fee / 100) / 1.16) * 0.007;
        this.respMensual = ((this.client.group.monthly_fee / 100) / 1.16) * 0.007;
      }
      this.totalExtras += this.respSocial;
      // this.sum = this.totalExtras + this.materialData.mensual + (this.client.group.sign_into.deposit / 100);
      this.sum = this.totalExtras + parseInt(this.materialData.mensual, 10) + (this.client.group.deposit / 100);
      this.sumaTotal.emit(this.sum);
    }
  }

  submit(): void {
    // build formdata and send it to the api
    const formData = new FormData();
    formData.append('client[name]', this.personalData.name);
    formData.append('client[address]', this.personalData.address);
    formData.append('client[phone]', this.personalData.phone);
    formData.append('client[email]', this.personalData.email);
    formData.append('client[between_streets]', this.personalData.between_streets);
    formData.append('client[colony]', this.personalData.colony);
    formData.append('client[state]', this.personalData.state);
    formData.append('client[county]', this.personalData.county);
    formData.append('client[postal_code]', this.personalData.postal_code);
    formData.append('client[id_groups]', this.client.group.id_groups);
    formData.append('responsabilidad', this.materialData.responsabilidad);
    formData.append('seller', this.client.id_clients);
    formData.append('monthly_installments', this.paymentData ? this.paymentData.monthly_installments : 1);
    formData.append('elements[]', this.materialData.material.id_categories_elements);

    for (const [key, value] of Object.entries(this.materialData.elements)) {
      formData.append('elements[]', key);
    }

    const pictures = {
      0: 'proof_of_address',
      1: 'id',
      2: 'profile',
      3: 'id_reverse'
    };
    for (const index in this.personalData.contractFotos) {
      if (index) {
        formData.append(name, this.personalData.contractFotos[index], pictures[index]);
      }
    }

    if (this.paymentData) {
      const datos = {
        name: this.client.name,
        number: this.paymentData.card_number,
        cvc: this.paymentData.cvc,
        exp_month: this.paymentData.expiration_month,
        exp_year: this.paymentData.expiration_year
      };
      Conekta.token.create({ card: datos },
        token => {
          formData.append('credit_card', token.id);
          this.register(formData);
        },
        response => {
          Swal.fire('Oops', 'Error al guardar la tarjeta en conekta, verifica que los datos de la tarjeta sean los correctos', 'error');
        }
      );
    } else {
      this.register(formData);
    }
  }

  register(formData): any {
    this.registerService.contract(formData).subscribe(res => {
      if (res.response && res.response.success) {
        Swal.fire('Correcto!', 'Contrato enviado exitosamente', 'success');
        formData.reset();

        return;
      }
      if (res.error && res.error.response.errors) {
        let errorsString = '';
        const errors = res.error.response.errors;
        for (const [key, value] of Object.entries(errors)) {
          errorsString += `${key}: ${value} \n`;
        }
        Swal.fire('Oops, encontramos los siguientes errores', errorsString, 'error');
      } else {
        Swal.fire('Oops', 'Error al enviar el contrato', 'error');
      }
    });
  }

  previous(): void {
    this.stepper.previous();
  }

}
