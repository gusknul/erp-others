import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RegisterService } from 'src/app/services/register.service';

declare var Conekta: any;
@Component({
  selector: 'app-referido',
  templateUrl: './referido.component.html',
  styleUrls: ['./referido.component.scss']
})
export class ReferidoComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  @Input() stepper;
  @Output() isLoading: EventEmitter<boolean> = new EventEmitter();
  @Output() loadClient: EventEmitter<any> = new EventEmitter();
  @Input() indexSelected: number;

  constructor(private readonly formBuilder: FormBuilder, private readonly registerService: RegisterService) {
    this.form = this.formBuilder.group({
      email: ['', Validators.email]
    });
  }

  ngOnInit(): void {
    //    Conekta.setPublicKey('key_eYvWV7gUaMyaN4iD');    
  }

  submit(): any {
    this.isLoading.emit(true);
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.form.value.email = this.form.value.email ? this.form.value.email : 'aguagratis@aguagente.com';
    if (this.form.value.email) {
      this.registerService.getClientByEmail(this.form.value.email).subscribe(res => {
        this.isLoading.emit(false);
        if (res.success && res.response.length > 0) {
          this.loadClient.emit(res.response[0]);
          this.stepper.next();
        } else if (res.success && res.response.length === 0) {
          Swal.fire('Oops', 'El email que ingresaste no es vendedor', 'error');
        } else {
          Swal.fire('Oops', 'Algo anda mal', 'error');
        }
      });
    }
  }

}
