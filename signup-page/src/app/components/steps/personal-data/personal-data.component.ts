import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss']
})
export class PersonalDataComponent implements OnInit {

  form: FormGroup;
  @Input() stepper;
  @Output() personalData: EventEmitter<any> = new EventEmitter();
  @Output() personalDataForm: EventEmitter<any> = new EventEmitter();
  submitted = false;
  urlImage6: any;
  urlImage7: any;
  urlImage8: any;
  urlImage9: any;
  fotos = [];

  constructor(private readonly formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      phone: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      address: ['', Validators.required],
      between_streets: ['', Validators.required],
      colony: ['', Validators.required],
      county: ['', Validators.required],
      state: ['', Validators.required],
      postal_code: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5)])],
      contractFotos: []
    });
  }

  ngOnInit(): void {
    this.personalDataForm.emit(this.form);
  }

  submit(): any {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.personalData.emit(this.form.value);
    this.stepper.next();
  }

  onFileChanged(event, index): void {
    this.getBase64(event.target.files[0]).then(result => {
      if (result) {
        this.fotos[index] = event.target.files[0];
        this.form.controls.contractFotos.setValue(this.fotos);
        switch (index) {
          case 0:
            this.urlImage6 = result;
            break;
          case 1:
            this.urlImage7 = result;
            break;
          case 2:
            this.urlImage8 = result;
            break;
          case 3:
            this.urlImage9 = result;
            break;
        }
      }
    });
  }

  imgToBase64(img): string {
    img.setAttribute('crossOrigin', 'anonymous');
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    const dataURL = canvas.toDataURL('image/png');

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  }

  async getBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => { resolve(reader.result); };
      reader.onerror = reject;
      reader.readAsDataURL(file);
    });
  }

  fileToBase64(file): any {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    const result = reader.onload = () => reader.result;
    reader.onerror = error => error;

    return result;
  }

  previous(): void {
    this.stepper.previous();
  }

}
