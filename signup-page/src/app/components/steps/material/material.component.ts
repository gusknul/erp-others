import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit, OnChanges {

  @Input() client: any;
  @Input() stepper;
  form: FormGroup;
  categories = [];
  elements = {};
  @Output() isLoading: EventEmitter<boolean> = new EventEmitter();
  @Output() materialData: EventEmitter<any> = new EventEmitter();

  constructor(private readonly formBuilder: FormBuilder, private readonly registerService: RegisterService) {
    this.form = this.formBuilder.group({
      mensual: '',
      responsabilidad: true,
      material: [],
      elements: this.elements
    });
  }

  ngOnInit(): void {
    //
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    if (simpleChanges.client.currentValue) {
      // this.form.controls.mensual.setValue(parseFloat(this.client.group.sign_into.monthly_fee) / 100);
      this.form.controls.mensual.setValue(parseFloat(this.client.group.monthly_fee) / 100);
      this.registerService.getCategories().subscribe(res => {
        if (res.success && res.response.length > 0) {
          this.categories = res.response;

          const material = res.response.filter(s => s.name === 'Material');
          if (material[0]) {
            const acero = material[0].elements.filter(s => s.name === 'Acero inoxidable');
            if (acero[0]) {
              // acero[0].price = parseFloat(this.client.group.sign_into.installation_fee) / 100;
              acero[0].price = parseFloat(this.client.group.installation_fee) / 100;
            }
          }
          this.form.controls.material.setValue(res.response ? res.response[0].elements[0] : []);
        } else {
          Swal.fire('Oops', 'Algo anda mal', 'error');
        }
      });
    }
  }

  submit(): void {
    this.materialData.emit(this.form.value);
    this.stepper.next();
  }

  previous(): void {
    this.stepper.previous();
  }

  get f(): any {
    return this.form.controls;
  }

  setExtras(event): void {
    if (this.elements[event.value.id_categories_elements]) {
      // tslint:disable-next-line: no-dynamic-delete
      delete this.elements[event.value.id_categories_elements];
    } else {
      this.elements[event.value.id_categories_elements] = event.value;
    }
    this.form.controls.elements.setValue(this.elements);
  }

}
