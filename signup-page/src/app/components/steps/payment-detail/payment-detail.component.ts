import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
declare var Conekta: any;
import Swal from 'sweetalert2';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss']
})
export class PaymentDetailComponent implements OnInit {

  form: FormGroup;
  @Input() stepper;
  @Output() paymentData: EventEmitter<any> = new EventEmitter();
  @Output() feesData: EventEmitter<any> = new EventEmitter();
  submitted = false;
  monthExpirations = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12'
  ];

  years = new Array();
  banks = [
    'VISA',
    'MASTERCARD',
    'AMEX'
  ];

  imgBrand = '';
  brandUppercase = '';
  monthlyInstallmentsOptions: any;
  fees: any;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly registerService: RegisterService) {
    this.form = this.formBuilder.group({
      card_number: '',
      cvc: '',
      expiration_month: '',
      expiration_year: '',
      monthly_installments: '',
      fee_installments: ''
    });
  }

  ngOnInit(): void {
    const date = new Date();
    for (let i = 0; i < 20; i++) {
      this.years.push((date.getUTCFullYear() + i).toString());
    }

    this.registerService.getFees().subscribe(res => {
      if (res.success) {
        const data = res.response;
        this.fees = data;
        this.feesData.emit(this.fees);
      }
    });
  }

  checkBrand(): void {
    const brand = Conekta.card.getBrand(this.form.controls.card_number.value);
    if (brand) {
      this.imgBrand = brand;
      switch (brand) {
        case 'visa':
          this.monthlyInstallmentsOptions = this.fees.VISA;
          break;
        case 'amex':
          this.monthlyInstallmentsOptions = this.fees.AMERICAN_EXPRESS;
          break;
        case 'mastercard':
          this.monthlyInstallmentsOptions = this.fees.MC;
          break;
        default:
          break;
      }
    } else {
      this.imgBrand = '';
    }
  }

  submit(): any {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    if (this.form.value.card_number) {
      const error = this.validarTarjeta(this.form.value);
      if (error) {
        Swal.fire('Oops', error, 'error');
      } else {
        this.paymentData.emit(this.form.value);
        this.stepper.next();
      }
    } else {
      this.paymentData.emit('');
      this.stepper.next();
    }
  }

  validarTarjeta(data): any {
    if (!Conekta.card.validateNumber(data.card_number)) { return 'Número de tarjeta invalido'; }
    if (!Conekta.card.validateCVC(data.cvc)) { return 'CVC incorrecto'; }
    if (!Conekta.card.validateExpirationDate(data.expiration_month, data.expiration_year)) { return 'Fecha de vencimiento invalida'; }

    return false;
  }

  previous(): void {
    this.stepper.previous();
  }

  changePago(value): void {
    if (value) {
      this.form.controls.fee_installments.setValue(this.monthlyInstallmentsOptions[value]);
    }
  }

}
