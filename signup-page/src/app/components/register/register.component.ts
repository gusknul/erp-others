import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
declare var Conekta: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  loading = false;
  client: any;
  personalData: any;
  materialData: any;
  paymentData: any;
  personalDataForm;
  selectedIndex: number;
  fees: any;
  total = 0;

  onSubmit(): boolean {
    return false;
  }

  ngOnInit(): void {
    Conekta.setPublishableKey('key_MaFzT3SvrAEBkonXV3gg2HQ'); // Sandbox
  }

  onLoading(isLoading): void {
    this.loading = isLoading;
  }

  setClient(client): void {
    this.client = client;
  }

  setPersonalData(personalData): void {
    this.personalData = personalData;
  }

  setMaterialData(materialData): void {
    this.materialData = materialData;
  }

  setPaymentData(paymentData): void {
    this.paymentData = paymentData;
  }

  stepChanged(event): void {
    this.selectedIndex = event.selectedIndex;
  }

  setPersonalDataForm(form): void {
    this.personalDataForm = form;
  }

  setFees(data): void {
    this.fees = data;
  }

  setSumaTotal(value): void {
    this.total = value;
  }

}
