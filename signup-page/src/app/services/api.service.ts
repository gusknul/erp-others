import { throwError as observableThrowError, ReplaySubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private readonly http: HttpClient) {
    }

    post(url: string, body: any): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.http.post<any>(
            `${environment.apiUrl}${url}`,
            body,
            {}
        ).pipe(
            map(res => ({
                    code: 200,
                    response: res
                })
            ),
            catchError((error: any) =>
                observableThrowError({
                    error: error.error
                })
            )
        ).subscribe(
            res => {
                response.next(res);
            },
            err => {
                response.next(err);
            }
        );

        return response;
    }

    get(url: string): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.http.get(`${environment.apiUrl}${url}`)
            .pipe(
                map(res => res), 
                catchError((error: any) =>
                    observableThrowError({
                        elementDeleted: false,
                        errorMessage: error
                    })
                )
            )
            .subscribe(
                res => {
                    response.next(res);
                },
                err => {
                    response.next(err);
                }
            );

        return response;
    }

    put(url: string, body: any): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.http.put(`${environment.apiUrl}${url}`, body)
            .pipe(
                map((resp: any) =>
                    ({
                        code: 200,
                        response: resp
                    })
                ),
                catchError((error: any) =>
                    observableThrowError({
                        code: error.status,
                        errorMessage: error.error,
                        detail: error.detail
                    })
                )
            )
            .subscribe(
                res => {
                    response.next(res);
                },
                err => {
                    response.next(err);
                }
            );

        return response;
    }

    destroy(url: string): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.http.delete(`${environment.apiUrl}${url}`)
            .pipe(
                map(res => res), 
                catchError((error: any) =>
                    observableThrowError({
                        code: error.status,
                        errorMessage: error.error,
                        detail: error.detail
                    })
                )
            )
            .subscribe(
                res => {
                    response.next(res);
                },
                err => {
                    response.next(err);
                }
            );

        return response;
    }
}
