import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    constructor(private readonly apiService: ApiService) { }

    getClientByEmail(email): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.apiService.get(`/clients?email=${email}`).subscribe(data => {
            response.next(data);
        });

        return response;
    }

    getCategories(): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.apiService.get('/categories').subscribe(data => {
            response.next(data);
        });

        return response;
    }

    getFees(): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.apiService.get('/fees').subscribe(data => {
            response.next(data);
        });

        return response;
    }

    contract(data): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.apiService.post('/contracts', data).subscribe(resp => {
            response.next(resp);
        });

        return response;
    }

}
