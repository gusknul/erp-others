import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { ReferidoComponent } from './components/steps/referido/referido.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './components/shared/footer/footer.component';
import { FieldComponent } from './components/utils/field/field.component';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { HttpClientModule } from '@angular/common/http';
import { MaterialComponent } from './components/steps/material/material.component';
import { MesesPipe } from './pipes/meses.pipe';
import { PersonalDataComponent } from './components/steps/personal-data/personal-data.component';
import { PaymentDetailComponent } from './components/steps/payment-detail/payment-detail.component';
import { ContractComponent } from './components/steps/contract/contract.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ReferidoComponent,
    RegisterComponent,
    FooterComponent,
    FieldComponent,
    MaterialComponent,
    MesesPipe,
    PersonalDataComponent,
    PaymentDetailComponent,
    ContractComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatStepperModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
